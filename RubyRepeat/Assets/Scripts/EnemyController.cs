using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed;
    public bool vertical = false;
    public float changeTime;
    public int Damage;
    public ParticleSystem smokeEffect;
    public ParticleSystem hitEffect;

    Rigidbody2D rigidbody2d;
    Animator animator;
    int direction = 1;
    bool broken = true;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        hitEffect.Stop();
        rigidbody2d = GetComponent<Rigidbody2D>();
        timer = changeTime;
        animator= GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!broken)
        {
            return;
        }
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            direction = -direction;
            timer = changeTime;
        }
    }
    private void FixedUpdate()
    {
        if (!broken)
        {
            return;
        }
        Vector2 position = rigidbody2d.position;
        if (vertical)
        {
            position.y = position.y + speed * Time.deltaTime * direction;
            animator.SetFloat("Move Y", direction);
            animator.SetFloat("Move X", 0);
        }
        else
        {
            position.x = position.x + speed * Time.deltaTime * direction;
            animator.SetFloat("Move X", direction);
            animator.SetFloat("Move Y", 0);
        }
        rigidbody2d.MovePosition(position);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {

        RubyController player = other.gameObject.GetComponent<RubyController>();
        if (player != null)
        {
            player.ChangeHealth(-Damage);
            direction = -direction;
        }
    }
    //Public because we want to call it from elsewhere like the projectile script
    public void Fix()
    {
        
        broken = false;
        rigidbody2d.simulated = false;
        animator.SetTrigger("Fixed");
        smokeEffect.Stop();
        hitEffect.Play();
        //Destroy(smokeEffect.gameObject);
    }
}
