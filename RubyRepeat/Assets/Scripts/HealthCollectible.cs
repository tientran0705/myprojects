using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectible : MonoBehaviour
{
    public AudioClip collectedClip;
    
   
    private void OnTriggerEnter2D(Collider2D other)
    {
        RubyController Controller = other.GetComponent<RubyController>();
        if (Controller != null)
        {
            if(Controller.health< Controller.maxHealth)
            {
                
                Controller.ChangeHealth(1);
               
                Destroy(gameObject);
                Controller.PlaySound(collectedClip);

            }
        }
    }
}
