using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "LayConfig", menuName = "GameConfiguration/LayConfig", order = 1)]
public class LayConfig : ScriptableObject
{
   [Header("Sprites of gameObjects")]
    public Sprite spr_Lay1;
    public Sprite spr_Lay2;
    public Sprite spr_Lay1_2;
    public Sprite spr_select1;
    public Sprite spr_select2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
