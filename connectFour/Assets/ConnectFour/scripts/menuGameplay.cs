using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuGameplay : MonoBehaviour
{
    [SerializeField]private GameObject Menu_1;
    [SerializeField]private GameObject Menu_2;

    public void EnableMenu()
    {
        Menu_1.SetActive(true);
        Menu_2.SetActive(true);
    }
    
    public void Resume()
    {
        Menu_1.SetActive(true);
        Menu_2.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
