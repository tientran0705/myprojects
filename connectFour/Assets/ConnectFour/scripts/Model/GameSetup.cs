using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetup : MonoBehaviour
{
    public const string RMS_GAMEMODE_SELECT = "GameModeSelect";

    public const string RMS_COLOR_SELECTED_ONE_PLAYER = "ColorSelectedOnePlayer";
    public const string RMS_TYPE_FIRST_MOVE_ONE_PLAY = "TypeFirstMoveOnePlay";
    public const string RMS_DIFFICULT_ONE_PLAY = "DifficultOnePlay";

    public const string RMS_COLOR_SELECTED_TWO_PLAYER = "ColorSelectedTwoPlayer";
    public const string RMS_TYPE_FIRST_MOVE_TWO_PLAY = "TypeFirstMoveTwoPlay";
    public const string RMS_DIFFICULT_TWO_PLAY = "DifficultTwoPlay";

    public const string RMS_NAME_PLAYER1_SINGLE_PLAYER = "NamePlayer1Single";
    public const string RMS_NAME_PLAYER2_SINGLE_PLAYER = "NamePlayer2Single";
    public const string RMS_NAME_PLAYER1_TWO_PLAYER = "NamePlayer1TwoPlayer";
    public const string RMS_NAME_PLAYER2_TWO_PLAYER = "NamePlayer2TwoPlayer";
    public  enum GAME_MODE{
        ONE_PLAY,TWO_PLAY
    }
    public enum COLOR_SELECT
    {
        YELLOW, ORANGE, GREEN
    }
    public enum FIRST_MOVE_TYPE
    {
        YELLOW, ORANGE, GREEN, RANDOMLY

    }
    public enum LEVEL_GAME
    {
        EASY, MEDIUM, HARD
    }
    public bool RequestNewGameOnePlayer = false;
    public bool RequestNewGameTwoPlayer = false;

    private string Player1ModeSinglePlayer = "Player";
    private string Player2ModeSinglePlayer = "Machine";
    private string Player1ModeTwoPlayer = "Player 1";
    private string Player2ModeTwoPlayer = "Player 2";

    private static GameSetup instance;

    private GameSetup() { }
    public static GameSetup Instance
    {
        get
        {
            if (instance == null)
            {
                GameSetup.Instance.Init();
                instance = new GameSetup();

            }
            return instance;
        }
    }
    void Init()
    {
        _gameModeSelected = PlayerPrefs.GetInt(RMS_GAMEMODE_SELECT)==0?GAME_MODE.ONE_PLAY:GAME_MODE.TWO_PLAY;
    }
    #region GAMEMODE
    private GAME_MODE _gameModeSelected = GAME_MODE.ONE_PLAY;
    public GAME_MODE GetCurrentSelectedGameMode()
    {
        return _gameModeSelected;
    }
    public void SetCurrentSelecteGameMode(GAME_MODE gameMode)
    {
        _gameModeSelected = gameMode;
        PlayerPrefs.SetInt(RMS_GAMEMODE_SELECT, _gameModeSelected==GAME_MODE.ONE_PLAY ? 0 : 1);
    }

    #endregion GAMEMODE

    #region COLOR_SELECT

    private COLOR_SELECT _colorSelected = COLOR_SELECT.YELLOW;
    public COLOR_SELECT GetCurrentColorSelected()
    {
        return _colorSelected;
    }
    public void SetCurrentColorSlected(COLOR_SELECT colorSelected)
    {
        _colorSelected = colorSelected;
        PlayerPrefs.SetInt(RMS_COLOR_SELECTED_ONE_PLAYER, _colorSelected == COLOR_SELECT.YELLOW ? 0 : _colorSelected==COLOR_SELECT.ORANGE?1:2);
    }
    #endregion
    #region FIRST_MOVE_TYPE
    private FIRST_MOVE_TYPE _firstMoveType = FIRST_MOVE_TYPE.YELLOW;
    public FIRST_MOVE_TYPE GetCurrentFirstMoveType()
    {
        return _firstMoveType;
    }
    public void SetCurrentFirstMoveType(FIRST_MOVE_TYPE firstMoveType)
    {
        _firstMoveType = firstMoveType;
        PlayerPrefs.SetInt(RMS_TYPE_FIRST_MOVE_ONE_PLAY, UpdatesFirstMove());
    }
    private int UpdatesFirstMove()
    {
        if(_firstMoveType == FIRST_MOVE_TYPE.YELLOW)
        {
            return 0;
        }
        else if(_firstMoveType == FIRST_MOVE_TYPE.ORANGE)
        {
            return 1;
        }
        else if(_firstMoveType == FIRST_MOVE_TYPE.GREEN)
        {
            return 2;
        }
        else
        {
            return 3;
        }
    }
    #endregion
    #region LEVEL_GAME
    private LEVEL_GAME _levelGame = LEVEL_GAME.EASY;
    public LEVEL_GAME GetCurrentLevelGame()
    {
        return _levelGame;
    }
    public void SetCurrentLevelGame(LEVEL_GAME levelGame)
    {
        _levelGame = levelGame;
        PlayerPrefs.SetInt(RMS_DIFFICULT_ONE_PLAY, _levelGame == LEVEL_GAME.EASY ? 0 : _levelGame == LEVEL_GAME.MEDIUM ? 1 : 2);
    }
    #endregion
}
