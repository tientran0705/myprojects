using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Thuy scripts
public class UIController : MonoBehaviour
{
    [Header("MAIN MENU")]
    public Button onePlayer;
    public Button twoPlayer;
    public Button stats;
    public Button options;

    [Header("GAME SELECTION MENU")]
    public Button backToMain;
    public Button newGame;
    public Button resumeGame;

    [Header("GAME OPTIONS MENU")]
    public Button backToMain_Op;
    //public Button material_Plastic;
    //public Button material_Wood;
    //public Button soundControl;
    public GameObject materialWood;
    public GameObject materialPlastic;
    [SerializeField] Toggle soundCtr;
    [SerializeField] Dropdown materials;
    private int saveSound;
    private string Game_Sound;
    private int saveMaterial;
    private string Game_Material;
        

    [Header("GAME MENUS")]
    public GameObject gameSelectionMenu;
    public GameObject gameLevelMenu;
    public GameObject gameOptionsMenu;
    public GameObject gameStats;
    public GameObject gameMainMenu;
    public Button moreGame;
    public Button info;
    public static bool isCheckmenuOne = false;
    private bool mainMenuActive = false;
    private bool onePlayerBtnOnClick = false;
    //private bool twoPlayerBtnOnClick = false;
    private transitionMenu player = new transitionMenu();



    // Start is called before the first frame update
    void Start()
    {
        onePlayer.onClick.AddListener(OnClickToOnePlayer);
        twoPlayer.onClick.AddListener(OnClickToTwoPlayer);
        stats.onClick.AddListener(OnClickStats);
        options.onClick.AddListener(OnClickOptions);
        moreGame.onClick.AddListener(MoreGame);
        info.onClick.AddListener(Information);
        backToMain.onClick.AddListener(OnClickMainMenu);
        newGame.onClick.AddListener(PlayerMenu);
        resumeGame.onClick.AddListener(OnClickToGamePlay);
        backToMain_Op.onClick.AddListener(OnClickMainMenu);

        //material_Plastic.onClick.AddListener(SwitchToPlastic);
        //material_Wood.onClick.AddListener(SwitchToWood);
        //soundControl.onClick.AddListener(SoundControll);
        if (PlayerPrefs.HasKey(Game_Sound))
        {
            saveSound = PlayerPrefs.GetInt(Game_Sound);
            if(saveSound == 1)
            {
                soundCtr.isOn = true;
            }
            else if(saveSound == 0)
            {
                soundCtr.isOn = false;
            }
        }
        if (PlayerPrefs.HasKey(Game_Material))
        {
            saveMaterial = PlayerPrefs.GetInt(Game_Material);
            if(saveMaterial == 0)
            {
                materials.value = 0;
            }
            else if(saveMaterial == 1)
            {
                materials.value = 1;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Options Menu Controller
    //public void SoundControll()
    //{
    //    Debug.Log("Mute / Unmuted");
    //}

    public void SwitchToPlastic()
    {
        materialPlastic.SetActive(true);
        materialWood.SetActive(false);
        //Debug.Log("Switch Plastic");
    }

    public void SwitchToWood()
    {
        materialWood.SetActive(true);
        materialPlastic.SetActive(false);
        //Debug.Log("Switch Wood");
    }

    //Game Selection Menu Controller
    public void PlayerMenu()
    {
        gameLevelMenu.SetActive(true);
        Debug.Log("open game Level");
    }

    public void OnClickToGamePlay()
    {
        Debug.Log("open gamePlay");
    }

    //Main Menu Controller
    public void MoreGame()
    {
        Debug.Log("More Game");
    }

    public void Information()
    {
        Debug.Log("Information");
    }
    public void OnClickMainMenu()
    {
        if (!mainMenuActive)
        {
            gameSelectionMenu.SetActive(false);
            gameStats.SetActive(false);
            gameOptionsMenu.SetActive(false);
        }
        gameMainMenu.SetActive(true);
        Debug.Log("to Main Menu");
    }

    public void OnClickToOnePlayer()
    {
        isCheckmenuOne = true;
        gameSelectionMenu.SetActive(true);
        onePlayerBtnOnClick = true;
        Debug.Log("to OnePlayer Menu");
    }

    public void OnClickToTwoPlayer()
    {
        isCheckmenuOne = false;
        gameSelectionMenu.SetActive(true);
        onePlayerBtnOnClick = false;
        Debug.Log("to twoPlayer Menu");
    }

    public void DifficultSelection()
    {
        if (!onePlayerBtnOnClick)
        {
            player.ChangeMainMenuToOnePlayerMenu();
        }
        player.ChangeMainMenuToTwoPlayerMenu();
    }

    public void OnClickStats()
    {
        gameStats.SetActive(true);
        Debug.Log("to Stats Menu");
    }

    public void OnClickOptions()
    {
        gameOptionsMenu.SetActive(true);
        Debug.Log("to Options Menu");
    }

    public void OnChangeToggleSound()
    {
        Debug.Log("Sound ON" + soundCtr.isOn);
        if(soundCtr.isOn)
        {
            saveSound = 1;
        }
        else
        {
            saveSound = 0;
        }
        PlayerPrefs.SetInt(Game_Sound, saveSound);
    }

    public void OnChangeMaterial()
    {
        Debug.Log("Change Material  " + materials.value);
        if(materials.value == 0)
        {
            SwitchToPlastic();
            saveMaterial = 0;
        }
        else
        {
            SwitchToWood();
            saveMaterial = 1;
        }
        PlayerPrefs.SetInt(Game_Material, saveMaterial);
    }
}

//end of Thuy
