using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu_Player_Controller1 : MonoBehaviour
{
    [Header("Button")]
    [SerializeField] Button Btn_Back;
    [SerializeField] Button Btn_Play;
    [SerializeField] GameObject MenuPlayer;
    [SerializeField] GameObject selector;
    [SerializeField] GameObject slider;

    [SerializeField] Image lay1;
    [SerializeField] Image lay2;
   
    [SerializeField] Image color_lay1;
    [SerializeField] Image color_lay2;  

    [SerializeField] Image select_lay1;
    [SerializeField] Image select_lay2;  
    [SerializeField] Image select_lay;

    [SerializeField] GameObject name_menu1;
    [SerializeField] GameObject name_menu2;

     Animator animator;
    [SerializeField] Button btn_lay_1;
    [SerializeField] Button btn_lay_0;
    [SerializeField] Button btn_lay_2;
    [SerializeField] Button Swap_lay;

    [SerializeField] InputField ipfName1;
    [SerializeField] InputField ipfName2;
    const string NAME_1 = "NAME_1";//save name player1
    const string NAME_2 = "NAME_2";//save name player2
    const string LAY_SELECT = "LAY_SELECT";//save select
    const string PLAYER_FIRST = "PLAYER_FIRST";//choose player first

    public LayConfig layConfig;
   
    bool swap = false;
    private void OnEnable()
    {
        ConfigPlayerMenu();
        
    }
    
    // Start is called before the first frame update
    void Start()
    {
        Btn_Back.onClick.AddListener(BackToMainMenu);
        Btn_Play.onClick.AddListener(PlayGame);

        Swap_lay.onClick.AddListener(swap_lay1_2);
        animator = selector.GetComponent<Animator>();
        btn_lay_2.onClick.AddListener(ChooseLay_2);
        btn_lay_1.onClick.AddListener(ChooseLay_1);
        btn_lay_0.onClick.AddListener(ChooseLay_0);

        if (PlayerPrefs.HasKey(LAY_SELECT))
        {
            if (PlayerPrefs.GetInt(LAY_SELECT) == 12)
            {
                lay1.sprite = layConfig.spr_Lay1;
                lay2.sprite = layConfig.spr_Lay2;
                color_lay1.sprite = layConfig.spr_Lay1;
                color_lay2.sprite = layConfig.spr_Lay2;
                Debug.Log("fdsfdsfsd");
            }
            else if (PlayerPrefs.GetInt(LAY_SELECT) == 21)
            {
                lay1.sprite = layConfig.spr_Lay2;
                lay2.sprite = layConfig.spr_Lay1;
                color_lay1.sprite = layConfig.spr_Lay2;
                color_lay2.sprite = layConfig.spr_Lay1;
            }
            else if (PlayerPrefs.GetInt(LAY_SELECT) == 122)
            {
                lay1.sprite = layConfig.spr_Lay1_2;
                lay2.sprite = layConfig.spr_Lay2;
                color_lay1.sprite = layConfig.spr_Lay1_2;
                color_lay2.sprite = layConfig.spr_Lay2;
            }
            else if (PlayerPrefs.GetInt(LAY_SELECT) == 212)
            {
                lay1.sprite = layConfig.spr_Lay2;
                lay2.sprite = layConfig.spr_Lay1_2;
                color_lay1.sprite = layConfig.spr_Lay2;
                color_lay2.sprite = layConfig.spr_Lay1_2;
            }
        }
        if (PlayerPrefs.HasKey(PLAYER_FIRST))
        {
            if (PlayerPrefs.GetInt(PLAYER_FIRST) == 1)
            {
                ChooseLay_1();
            }
            else if (PlayerPrefs.GetInt(PLAYER_FIRST) == 0)
            {
                ChooseLay_0();
            }
            else if (PlayerPrefs.GetInt(PLAYER_FIRST) == 2)
            {
                ChooseLay_2();
            }
        }




    }

    // Update is called once per frame
    void Update()
    {

       
    }
    public void ChooseLay_1()
    {
        animator.SetTrigger("Select1");//play c�i select_1
        PlayerPrefs.SetInt(PLAYER_FIRST, 1);//lay1 first
    }
    
    public void ChooseLay_0()
    {
        animator.SetTrigger("Select0");//play random 
        PlayerPrefs.SetInt(PLAYER_FIRST, 0);//lay random first

    }
    
    public void ChooseLay_2()
    {
        animator.SetTrigger("Select2"); //play c�i select_2
        PlayerPrefs.SetInt(PLAYER_FIRST, 2);//lay2 first
    }
    public void swap_lay1_2()
    {
        if (UIController.isCheckmenuOne == true)
        {
            if (swap == false)
            {
                PlayerPrefs.SetInt(LAY_SELECT, 21);//Lay2 was selected
                lay1.sprite = layConfig.spr_Lay2;
                lay2.sprite = layConfig.spr_Lay1;
                color_lay1.sprite = layConfig.spr_Lay2;
                color_lay2.sprite = layConfig.spr_Lay1;
                swap = true;
            }
            else
            {
                PlayerPrefs.SetInt(LAY_SELECT, 12);//Lay1 was selected
                lay1.sprite = layConfig.spr_Lay1;
                lay2.sprite = layConfig.spr_Lay2;
                color_lay1.sprite = layConfig.spr_Lay1;
                color_lay2.sprite = layConfig.spr_Lay2;
                swap = false;
            }
        }
        else swap_lay1_2_2();
        
       
    }
    public void swap_lay1_2_2()
    {
        if (swap == false)
        {
            PlayerPrefs.SetInt(LAY_SELECT, 212);//Lay2 was selected
            lay1.sprite = layConfig.spr_Lay2;
            lay2.sprite = layConfig.spr_Lay1_2;
            color_lay1.sprite = layConfig.spr_Lay2;
            color_lay2.sprite = layConfig.spr_Lay1_2;
            swap = true;
        }
        else
        {
            PlayerPrefs.SetInt(LAY_SELECT, 122);//Lay1_2 was selected
            lay1.sprite = layConfig.spr_Lay1_2;
            lay2.sprite = layConfig.spr_Lay2;
            color_lay1.sprite = layConfig.spr_Lay1_2;
            color_lay2.sprite = layConfig.spr_Lay2;
            swap = false;
        }
        
    }
    public void ConfigPlayerMenu()
    {
        if (UIController.isCheckmenuOne == true)
        {
            lay1.sprite = layConfig.spr_Lay1;
            lay2.sprite = layConfig.spr_Lay2;
            color_lay1.sprite = layConfig.spr_Lay1;
            color_lay2.sprite = layConfig.spr_Lay2;
            select_lay1.sprite = layConfig.spr_Lay1;
            select_lay2.sprite = layConfig.spr_Lay2;
            select_lay.sprite = layConfig.spr_select1;
            slider.SetActive(true);
            name_menu1.SetActive(true);
            name_menu2.SetActive(false);
        }
        else
        {
            lay1.sprite = layConfig.spr_Lay1_2;
            lay2.sprite = layConfig.spr_Lay2;
            color_lay1.sprite = layConfig.spr_Lay1_2;
            color_lay2.sprite = layConfig.spr_Lay2;
            select_lay1.sprite = layConfig.spr_Lay1_2;
            select_lay2.sprite = layConfig.spr_Lay2;
            select_lay.sprite = layConfig.spr_select2;
            slider.SetActive(false);
            name_menu1.SetActive(false);
            name_menu2.SetActive(true);
        }
    }
    public void saveName()
    {
        PlayerPrefs.SetString(NAME_1, ipfName1.text);// save name of player 1
        PlayerPrefs.SetString(NAME_2, ipfName2.text);// save name of player 2
    }
    public void BackToMainMenu()
    {
        //MenuMain.SetActive(true);
        MenuPlayer.SetActive(false);

    }
    public void PlayGame()
    {
        SceneManager.LoadScene("GamePlay");
    }



}
