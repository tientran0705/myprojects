﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Binary_Tree
    {
        public Binary_Tree() { }
        public static Binary_Tree Instance;
        private void CreateSubTree(Stack<Binary_Tree_Node> opStack, Stack<Binary_Tree_Node> nodeStack)
        {
            Binary_Tree_Node node = (Binary_Tree_Node)opStack.Pop();
            node.RightChild = (Binary_Tree_Node)nodeStack.Pop();
            node.LeftChild = (Binary_Tree_Node)nodeStack.Pop();
            nodeStack.Push(node);
        }

        public Binary_Tree_Node Infix2ExpressionTree(string infixExpression)
        {
            List<string> prefix = new List<string>();
            Stack<Binary_Tree_Node> operatorStack = new Stack<Binary_Tree_Node>();
            Stack<Binary_Tree_Node> nodeStack = new Stack<Binary_Tree_Node>();

            infixExpression = FormatExpression(infixExpression);

            string[] tokens = infixExpression.Split(' ').ToArray();

            for (int i = 0; i < tokens.Count(); i++)
            {
                if (is_operator(tokens[i]))
                {
                    if ((i == 0) || (i > 0 && (is_operator(tokens[i - 1]) || tokens[i - 1] == "(")))
                    {
                        if (tokens[i] == "-")
                        {
                            nodeStack.Push(new Binary_Tree_Node(tokens[i] + tokens[i + 1]));
                            i++;
                        }
                    }
                    else
                    {
                        while (operatorStack.Count > 0 && GetPriority(operatorStack.Peek().Value) >= GetPriority(tokens[i]))
                            CreateSubTree(operatorStack, nodeStack);

                        operatorStack.Push(new Binary_Tree_Node(tokens[i]));
                    }
                }


                else if (tokens[i] == "(")
                    operatorStack.Push(new Binary_Tree_Node(tokens[i]));
                else if (tokens[i] == ")")
                {
                    while (operatorStack.Peek().Value != "(")
                        CreateSubTree(operatorStack, nodeStack);
                    operatorStack.Pop();
                }
                else
                    nodeStack.Push(new Binary_Tree_Node(tokens[i]));
            }

            while (operatorStack.Count > 0)
                CreateSubTree(operatorStack, nodeStack);

            return nodeStack.Peek();
        }

        public float EvaluateExpressionTree(Binary_Tree_Node node)
        {
            float t = 0;
            if (node.IsLeaf)
                t = float.Parse(node.Value);
            else
            {
                float x = EvaluateExpressionTree(node.LeftChild);
                float y = EvaluateExpressionTree(node.RightChild);

                switch (node.Value)
                {
                    case "+": t = x + y; break;
                    case "-": t = x - y; break;
                    case "*": t = x * y; break;
                    case "/": t = x / y; break;
                    case "%": t = x % y; break;
                }
            }
            return t;
        }

        public int GetPriority(string op)
        {
            if (op == "*" || op == "/" || op == "%" || op == "^")
                return 2;
            if (op == "+" || op == "-")
                return 1;
            return 0;
        }
        public bool is_operator(string str)
        {
            if (str == "+" || str == "-" || str == "*" || str == "/")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool is_operand(string str)
        {
            int temp;
            return int.TryParse(str, out temp) ? true : false;
        }

        public string FormatExpression(string expression)
        {
            expression = expression.ToLower().Replace(" ", "");
            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%|\^){3,}", match => match.Value[0].ToString());

            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%|\^)(\+|\*|\/|\%|\^)", match =>
                match.Value[0].ToString()
            );
            expression = Regex.Replace(expression, @"\+|\-|\*|\/|\%|\^|\)|\(", match =>
                String.Format(" {0} ", match.Value)
            );
            expression = expression.Replace("  ", " ");
            expression = expression.Trim();

            return expression;
        }
    }
}
