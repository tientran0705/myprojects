﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Binary_Tree_Node
    {
        public Binary_Tree_Node LeftChild;
        public Binary_Tree_Node RightChild;
        public string Value;

        public bool IsLeaf
        {
            get { return this.LeftChild == null && this.RightChild == null; }
        }

        public Binary_Tree_Node(string value)
        {
            Value = value;
        }


    }
}
