using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;


public class Cal_num : MonoBehaviour
{
    public GameObject cal_result;
    public Text txt_error;

    bool check_error_final(string s)
    {
        if (s[s.Length - 1] == '-' || s[s.Length - 1] == '+' || s[s.Length - 1] == '*' || s[s.Length - 1] == '/')
        {
            return true;
        }
        return false;
    }
    bool Compare(string s1, string s2)
    {
        return string.Compare(s1, s2) == 0;
    }
    string exp_error()
    {
        txt_error.enabled = true;
        return "Eorror";
    }
    public void calculator()
    {
        string expression_list = cal_result.GetComponent<Text>().text;
        if (expression_list == "") { return; }
        if (check_error_final(expression_list))
        {
            expression_list = "";
            cal_result.GetComponent<Text>().text = "";
            exp_error();
            return;
        }
        string str_expression = String.Join("", expression_list);
        Binary_Tree bin = new Binary_Tree();
        Binary_Tree_Node tree = bin.Infix2ExpressionTree(str_expression);
        float result = bin.EvaluateExpressionTree(tree);
        txt_error.enabled = false;
        if (Compare(expression_list, result.ToString()))
        {
            return;
        }
        if (result.ToString() == "Infinity" || result.ToString() == "-Infinity")
        {
            expression_list = "";
            cal_result.GetComponent<Text>().text = "";
            exp_error();
            return;
        }

        cal_result.GetComponent<Text>().text = result.ToString();
    }
}
