﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Del_text : MonoBehaviour
{
    public GameObject txt_cal;
    public void Del_Text()
    {
        string str_cal = txt_cal.GetComponent<Text>().text;
        if (str_cal != "")
        {
            str_cal = str_cal.Remove(str_cal.Length - 1, 1);
            txt_cal.GetComponent<Text>().text = str_cal;
        }


    }
}

